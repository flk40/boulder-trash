extends PathFollow2D

var speed = 0.5

func _draw():
	$Player.playing=true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if unit_offset < 1 :
		unit_offset += delta*speed
	else:
		$Player.animation = "Idle"

func _on_Timer_timeout():
	var rock = get_node("../../Path2DRock/PathFollow2D")
	if rock:
		rock.speed = 2.0
