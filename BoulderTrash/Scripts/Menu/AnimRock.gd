extends PathFollow2D
var speed = 0.0

func _draw():
	$Rock.playing=true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if unit_offset < 1 :
		unit_offset += delta*speed
	else:
		$Rock.animation = "Bottom"
		var player = get_node("../../Path2DPlayer/PathFollow2D/Player")
		if player:
			player.hide()
			$Blood.show()
			$Blood.playing = true


func _on_Blood_animation_finished():
	var trash = get_node("../../Trash")
	if trash:
		trash.show()
	var tween = get_node("../../TweenStart")
	if tween:
		tween.start()
		get_node("../../").lockTween = true

