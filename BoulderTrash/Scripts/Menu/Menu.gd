extends Node2D

var lockTween:bool = false

func _draw():
	var delayS=00
	var durationS = 0.4
	var delayE=00
	var durationE = 0.4
	# Zoom avant/apres START
	$TweenStart.interpolate_property($Start, "scale", Vector2(.8,.8),Vector2(1,1), durationS, Tween.TRANS_LINEAR, Tween.EASE_IN,delayS)
	delayS+=durationS
	$TweenStart.interpolate_property($Start, "scale", Vector2(1,1),Vector2(.8,.8), durationS, Tween.TRANS_LINEAR, Tween.EASE_IN,delayS)
	delayS+=durationS
	$TweenStart.repeat=true

	# Zoom avant/apres START
	$TweenExit.interpolate_property($Exit, "scale", Vector2(.8,.8),Vector2(1,1), durationE, Tween.TRANS_LINEAR, Tween.EASE_IN,delayE)
	delayE+=durationE
	$TweenExit.interpolate_property($Exit, "scale", Vector2(1,1),Vector2(.8,.8), durationE, Tween.TRANS_LINEAR, Tween.EASE_IN,delayE)
	delayE+=durationE
	$TweenExit.repeat=true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):

	if Input.is_action_pressed("ui_cancel") :
		get_tree().quit()

	# Passage en mode plein écran
	if Input.is_action_pressed("ui_f"):
		OS.window_fullscreen = not OS.window_fullscreen
	# Actionne le zoom sur le menu selectionné
	if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_up"):
		if lockTween:
			if $TweenStart.is_active():
				$TweenStart.stop_all()
				$TweenStart.reset_all()
			else:
				$TweenStart.start()

			if $TweenExit.is_active():
				$TweenExit.stop_all()
				$TweenExit.reset_all()
			else:
				$TweenExit.start()
	if Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("ui_select"):
		# Lance le jeu ou quitte le jeu
		if lockTween:
			if $TweenStart.is_active():
				var _value = get_tree().change_scene_to(load("res://Scenes/Level.tscn"))
			else:
				get_tree().quit()
